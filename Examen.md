    Un dels riscos que he detectat es el fet de que pugues caure al teu cap objectes tals com TPVs, Impresoras u 
    altres objectes d'amunt.

    Altre risc que he vist es el terra, el qual estaba amb desnivels i et podias caure, inclus morir.

    no hi ha sortida d'emergencia, es a dir, si hi ha un incendi, i tothom sort corrent del lloc, es 
    pot produir un atac de DDos a la porta, es a dir, que de tanta gent es pot quedar bloquejat, igual
    no es podria surtir si el foc es genera ahi, etc.

    no hi ha ventlació, i pot haber-hi molt co2.

    está tot ple de equips antics, i et pots electrocutar, cortar o terminar amb la vida del equip en questió.
